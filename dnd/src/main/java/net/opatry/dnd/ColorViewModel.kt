package net.opatry.dnd

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.VisibleForTesting
import androidx.core.graphics.ColorUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlin.random.Random

data class ColorPill(val index: Int, @ColorInt val color: Int, val isSelected: Boolean = false) {
    override fun toString(): String {
        val colorString = "#${String.format("%08x", color)} @$index"
        return if (isSelected) "$colorString ✓" else colorString
    }
}

private fun List<Int>.toColorPills(selectedIndex: Int) = mapIndexed { index, c -> ColorPill(index, c, index == selectedIndex) }

class ColorViewModel(private val colorPreferences: ColorPreferences) : ViewModel() {

    private var selectedIndex: Int = 0

    @ColorInt
    private var rawColors: List<Int> = listOf()
        set(value) {
            field = value
            viewModelScope.launch {
                _colors.value = value.toColorPills(selectedIndex)
            }
        }

    private val _colors = MutableLiveData<List<ColorPill>>()
    val colors: LiveData<List<ColorPill>>
        get() = _colors

    init {
        rawColors = colorPreferences.colors
    }

    fun resetColors(@ColorInt newColors: List<Int>) {
        selectedIndex = if (newColors.isEmpty()) UNSELECTED else 0
        rawColors = colorPreferences.resetColors(newColors)
    }

    fun selectColorAt(index: Int) {
        selectedIndex = if (index in rawColors.indices) {
            index
        } else {
            UNSELECTED
        }
        rawColors = rawColors // Force LiveData update FIXME not very nice
    }

    fun selectColor(@ColorInt color: Int) {
        if (selectedIndex != UNSELECTED) {
            // keep selected color if already the right one (especially useful when having duplicated colors)
            if (rawColors[selectedIndex] == color) return
            selectColorAt(rawColors.indexOf(color))
        }
    }

    fun removeColorAt(index: Int) {
        if (index in rawColors.indices) {
            val newColors = colorPreferences.removeColorAt(index)
            selectedIndex = if (newColors.isEmpty()) UNSELECTED else (--selectedIndex).coerceAtLeast(0)
            rawColors = newColors
        }
    }

    fun removeColor(@ColorInt color: Int) {
        removeColorAt(rawColors.indexOf(color))
    }

    fun removeSelectedColor() {
        removeColorAt(selectedIndex)
    }

    fun addColor(@ColorInt color: Int = randomColor) {
        val newColors = colorPreferences.addColor(color)
        selectedIndex = newColors.size - 1
        rawColors = newColors
    }

    fun duplicateSelectedColor() {
        val newColors = colorPreferences.duplicateColorAt(selectedIndex)
        ++selectedIndex
        rawColors = newColors
    }

    fun moveColor(fromPosition: Int, toPosition: Int) {
        if (fromPosition == toPosition) return

        val newColors = colorPreferences.moveColors(fromPosition, toPosition)

        @Suppress("ReplaceRangeToWithUntil")
        selectedIndex = when (selectedIndex) {
            // moving current selection
            fromPosition -> toPosition
            // selected item between source and destination (from < select <= to)
            in (fromPosition + 1)..toPosition -> selectedIndex - 1
            // selected item between source and destination (to <= select < from)
            in toPosition..(fromPosition - 1) -> selectedIndex + 1
            else -> selectedIndex
        }

        rawColors = newColors
    }

    @VisibleForTesting
    public override fun onCleared() {
        super.onCleared()
    }

    companion object {
        private const val UNSELECTED = -1

        @get:ColorInt
        val randomColor: Int
            get() {
                // restrict the range of acceptable values based on saturation and value (brightness)
                val hue = Random.nextInt(360)
                val saturation = Random.nextFloat() + .5f // between .5 and 1
                val value = Random.nextFloat() + .5f // between .5 and 1
                val hsv = floatArrayOf(hue.toFloat(), saturation, value)
                return ColorUtils.setAlphaComponent(Color.HSVToColor(hsv), 0xff)
            }
    }
}

class ColorViewModelFactory(private val colorPreferences: ColorPreferences) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return when {
            modelClass.isAssignableFrom(ColorViewModel::class.java) -> {
                ColorViewModel(colorPreferences) as T
            }
            else -> throw IllegalArgumentException("Unknown ViewModel $modelClass")
        }
    }
}
