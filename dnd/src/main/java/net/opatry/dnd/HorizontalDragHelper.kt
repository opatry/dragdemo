package net.opatry.dnd

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Canvas
import android.util.TypedValue
import androidx.core.animation.doOnEnd
import androidx.core.view.postDelayed
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView


class HorizontalDragHelper(private val isLocked: (RecyclerView.ViewHolder) -> Boolean = { false }, private val onDrop: (fromPosition: Int, toPosition: Int) -> Unit) : ItemTouchHelper(object : ItemTouchHelper.Callback() {
    private var itemAnimator: RecyclerView.ItemAnimator? = null
    private var sourcePosition: Int = RecyclerView.NO_POSITION
    private var originalElevation: Float = -1f

    override fun isLongPressDragEnabled() = true
    override fun isItemViewSwipeEnabled() = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) = Unit

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        // store initial item animator to allow disabling/enabling at will on drop
        if (itemAnimator == null) {
            itemAnimator = recyclerView.itemAnimator
        }

        val dragFlags = START or END
        val swipeFlags = 0
        return if (isLocked(viewHolder)) 0 else makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder) = true

    override fun canDropOver(recyclerView: RecyclerView, current: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        if (isLocked(target)) return false
        return super.canDropOver(recyclerView, current, target)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        if (actionState == ACTION_STATE_DRAG && isCurrentlyActive && originalElevation < 0f) {
            onDragViewStart(recyclerView, viewHolder)
        }
    }

    override fun onMoved(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, fromPos: Int, target: RecyclerView.ViewHolder, toPos: Int, x: Int, y: Int) {
        super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y)
        if (fromPos != toPos) {
            if (sourcePosition == RecyclerView.NO_POSITION) {
                sourcePosition = fromPos
            }
            recyclerView.adapter?.notifyItemMoved(fromPos, toPos)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        val target = viewHolder.adapterPosition
        if (sourcePosition != RecyclerView.NO_POSITION && sourcePosition != target && target != RecyclerView.NO_POSITION) {
            onDropView(recyclerView, viewHolder, sourcePosition, target)
        } else {
            onCancel(recyclerView, viewHolder)
        }
    }

    private fun onDragViewStart(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {
            originalElevation = elevation
            val targetElevation = originalElevation + TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, context.resources.displayMetrics)
            AnimatorSet().apply {
                playTogether(
                        ObjectAnimator.ofFloat(this@with, "elevation", targetElevation),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleX", 1.1f),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleY", 1.1f)
                )
                duration = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
                start()
            }
        }
    }

    private fun onDropView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, source: Int, target: Int) {
        with(viewHolder.itemView) {
            AnimatorSet().apply {
                playTogether(
                        ObjectAnimator.ofFloat(this@with, "elevation", originalElevation),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleX", 1f),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleY", 1f)
                )
                duration = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
                start()
                doOnEnd {
                    // disabled item animator on drop and restore it after a delay to avoid unfortunate fade-in fade-out animations
                    recyclerView.itemAnimator = null
                    recyclerView.postDelayed(100) {
                        recyclerView.itemAnimator = itemAnimator
                    }
                    onDrop(source, target)
                    sourcePosition = RecyclerView.NO_POSITION
                    originalElevation = -1f
                }
            }
        }
    }

    private fun onCancel(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        with(viewHolder.itemView) {
            AnimatorSet().apply {
                playTogether(
                        ObjectAnimator.ofFloat(this@with, "elevation", originalElevation),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleX", 1f),
                        ObjectAnimator.ofFloat(viewHolder.itemView, "scaleY", 1f)
                )
                duration = context.resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
                start()
                doOnEnd {
                    sourcePosition = RecyclerView.NO_POSITION
                    originalElevation = -1f
                }
            }
        }
    }
})
