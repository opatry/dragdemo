package net.opatry.dnd

import android.animation.ValueAnimator
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnCancel
import androidx.core.animation.doOnEnd
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView


class DragActivity : AppCompatActivity() {

    private val onColorInteraction = object : ColorPillViewHolder.Callback {
        override fun onColorClicked(item: ColorPill, position: Int) {
            if (item.isSelected) {
                Toast.makeText(this@DragActivity, "COLOR PICKER #${Integer.toHexString(item.color)}", Toast.LENGTH_SHORT).show()
            } else {
                colorViewModel.selectColorAt(position)
            }
        }
    }

    private val colorsAdapter = ColorListAdapter(onColorInteraction)

    private lateinit var colorViewModel: ColorViewModel
    private val colorDragHelper = HorizontalDragHelper(isLocked = { it.adapterPosition == 0 }) { source, target ->
        colorViewModel.moveColor(source, target)
    }
    private var resetColorsBtn: View? = null
    private var addColorBtn: View? = null
    private var duplicatedSelectedColorBtn: View? = null
    private var deleteSelectedColorBtn: View? = null
    private var unselectColorBtn: View? = null
    private var colorsList: RecyclerView? = null

    @get:ColorInt
    private val defaultColors: List<Int>
        get() = resources.getIntArray(R.array.predefined_ink_colors).toList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drag_activity)

        val factory = ColorViewModelFactory(ColorPreferences(application, defaultColors))
        colorViewModel = ViewModelProvider(this, factory)[ColorViewModel::class.java]
        colorViewModel.colors.observe(this) { colors ->
            val hasSelection = colors.any { it.isSelected }
            duplicatedSelectedColorBtn?.isEnabled = hasSelection
            deleteSelectedColorBtn?.isEnabled = hasSelection
            unselectColorBtn?.isEnabled = hasSelection
            val sizeChanged = colorsAdapter.itemCount != colors.size
            colorsAdapter.submitList(colors)
            if (sizeChanged) {
                val selectedIndex = colors.indexOfFirst { it.isSelected }
                if (selectedIndex != -1) {
                    colorsList?.smoothScrollToPosition(selectedIndex)
                }
            }

            with (findViewById<View>(R.id.color_preview)) {
                val previous = (background as? ColorDrawable)?.color ?: Color.TRANSPARENT
                val selectedColorPill = colors.firstOrNull { it.isSelected }
                val current = selectedColorPill?.color ?: Color.TRANSPARENT
                ValueAnimator.ofArgb(previous, current).apply {
                    duration = 500L
                    addUpdateListener { animator -> setBackgroundColor(animator.animatedValue as Int) }
                    doOnEnd { removeAllUpdateListeners() }
                    doOnCancel { removeAllUpdateListeners() }
                    start()
                }
            }
        }

        colorsList = findViewById<RecyclerView>(R.id.colors_list).apply {
            adapter = colorsAdapter
        }.also {
            colorDragHelper.attachToRecyclerView(it)
        }

        resetColorsBtn = findViewById(R.id.reset_colors)
        addColorBtn = findViewById(R.id.add_color)
        duplicatedSelectedColorBtn = findViewById(R.id.duplicate_selected_color)
        deleteSelectedColorBtn = findViewById(R.id.delete_selected_color)
        unselectColorBtn = findViewById(R.id.unselect_color)
    }

    override fun onStart() {
        super.onStart()
        resetColorsBtn?.setOnClickListener { colorViewModel.resetColors(defaultColors) }
        addColorBtn?.setOnClickListener { colorViewModel.addColor() }
        duplicatedSelectedColorBtn?.setOnClickListener { colorViewModel.duplicateSelectedColor() }
        deleteSelectedColorBtn?.setOnClickListener { colorViewModel.removeSelectedColor() }
        unselectColorBtn?.setOnClickListener { colorViewModel.selectColor(Color.TRANSPARENT) }
    }

    override fun onStop() {
        super.onStop()
        resetColorsBtn?.setOnClickListener(null)
        addColorBtn?.setOnClickListener(null)
        duplicatedSelectedColorBtn?.setOnClickListener(null)
        deleteSelectedColorBtn?.setOnClickListener(null)
        unselectColorBtn?.setOnClickListener(null)
    }
}
