package net.opatry.dnd

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Paint
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.Shape
import android.util.AttributeSet
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.CompoundButton
import androidx.annotation.ColorInt
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.ColorUtils

private fun Canvas.mutate(block: (Canvas) -> Unit) {
    save()
    try {
        block(this)
    } finally {
        restore()
    }
}

class ColorPillView(context: Context, attrs: AttributeSet) : CompoundButton(context, attrs) {

    private val shape: Shape

    private val fillPaint: Paint
    private val borderPaint: Paint

    private val checkDrawable: Drawable?

    @ColorInt
    var tint: Int = Color.TRANSPARENT
        set(value) {
            // We ignore alpha channel, colored layer is always fully opaque.
            // Special case for transparent having a special meaning (do not apply tint)
            val opaqueValue = if (value != Color.TRANSPARENT) ColorUtils.setAlphaComponent(value, 0xff) else value
            if (opaqueValue == field) return
            field = opaqueValue

            fillPaint.color = field

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                outlineSpotShadowColor = opaqueValue
            }

            invalidate()
        }

    init {
        with(context.theme.obtainStyledAttributes(attrs, R.styleable.ColorPillView, 0, 0)) {
            try {
                borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
                    style = Paint.Style.STROKE
                    strokeWidth = getDimension(R.styleable.ColorPillView_cpv_borderWidth, 0f)
                    color = getColor(R.styleable.ColorPillView_cpv_borderTint, Color.TRANSPARENT)
                }
                val pillTint = getColor(R.styleable.ColorPillView_cpv_pillTint, Color.TRANSPARENT)
                fillPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
                    style = Paint.Style.FILL
                    color = pillTint
                }
                tint = pillTint
                val drawable = getDrawable(R.styleable.ColorPillView_cpv_checkDrawable)
                checkDrawable = if (isInEditMode && drawable == null) {
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_check, context.theme)
                } else {
                    drawable
                }
            } finally {
                recycle()
            }
        }

        shape = OvalShape().apply {
            resize(width - paddingLeft - paddingRight, height - paddingTop - paddingBottom)
        }

        if (isInEditMode) {
            if (fillPaint.color == Color.TRANSPARENT)
                fillPaint.color = Color.YELLOW

            if (borderPaint.color == Color.TRANSPARENT)
                borderPaint.color = ColorUtils.setAlphaComponent(Color.BLACK, 0xa0)

            if (foregroundTintList == null)
                foregroundTintList = ColorStateList.valueOf(ColorUtils.setAlphaComponent(Color.BLACK, 0x80))
        }

        outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                if (outline != null) {
                    shape.getOutline(outline)
                    outline.offset(paddingLeft, paddingTop)
                }
            }
        }
        clipToOutline = true
    }

    fun setChecked(checked: Boolean, animated: Boolean) {
        isChecked = checked
        if (animated && checked) {
            (checkDrawable as? Animatable)?.start()
        }
    }

    override fun toggle() {
        // Do not uncheck checked color pill (like a radio button)
        if (!isChecked) {
            super.toggle()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        shape.resize((w - paddingLeft - paddingRight).toFloat(), (h - paddingTop - paddingBottom).toFloat())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.mutate { c ->
            c.translate(paddingStart.toFloat(), paddingTop.toFloat())
            shape.draw(c, fillPaint)
            shape.draw(c, borderPaint)
        }

        checkDrawable?.mutate()?.let { drawable ->
            // apply padding twice to offset drawable within circle
            drawable.setBounds(paddingLeft * 2, paddingTop * 2, width - (paddingRight * 2), height - (paddingTop * 2))
            drawable.setTint(foregroundTintList?.getColorForState(drawableState, Color.MAGENTA) ?: Color.MAGENTA)
            drawable.draw(canvas)
        }
    }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        return if (isChecked) {
            val states = super.onCreateDrawableState(extraSpace + 1)
            val brightnessState = getBrightnessAwareValue(tint, R.attr.state_on_bright_color, -R.attr.state_on_bright_color)
            mergeDrawableStates(states, intArrayOf(brightnessState))
            states
        } else {
            super.onCreateDrawableState(extraSpace)
        }
    }

    override fun getAccessibilityClassName() = ColorPillView::class.java.name
}
