package net.opatry.dnd

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView


class ColorPillViewHolder(parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.color_pill_view, parent, false)) {

    interface Callback {
        fun onColorClicked(item: ColorPill, position: Int)
    }

    private val colorPillView: ColorPillView = itemView.findViewById(R.id.color_pill_view)

    fun bind(colorPill: ColorPill, callback: Callback, payloads: MutableList<Any>) {
        val selectedStateUpdate = payloads.any { it == SELECTED_STATE_UPDATE }
        with(colorPillView) {
            if (!selectedStateUpdate) {
                tint = colorPill.color
            }
            setChecked(colorPill.isSelected, selectedStateUpdate)
        }

        // avoids selecting pill on a "canceled" drag
        if (!colorPill.isSelected) {
            itemView.setOnLongClickListener { true }
        }
        itemView.setOnClickListener {
            callback.onColorClicked(colorPill, adapterPosition)
        }
    }

    fun unbind() {
        itemView.setOnLongClickListener(null)
        itemView.setOnClickListener(null)
    }

    companion object {
        internal const val SELECTED_STATE_UPDATE = "SELECTED_STATE_UPDATE"
    }
}

class ColorListAdapter(private val callback: ColorPillViewHolder.Callback) : ListAdapter<ColorPill, ColorPillViewHolder>(COLOR_PILL_DIFF_UTIL_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ColorPillViewHolder(parent)

    override fun onBindViewHolder(holder: ColorPillViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun onBindViewHolder(holder: ColorPillViewHolder, position: Int, payloads: MutableList<Any>) {
        holder.bind(getItem(position), callback, payloads)
    }

    override fun onViewRecycled(holder: ColorPillViewHolder) {
        holder.unbind()
        super.onViewRecycled(holder)
    }

    override fun submitList(list: List<ColorPill>?) {
        if (list == null) {
            super.submitList(null)
        } else {
            super.submitList(ArrayList(list)) // force DiffUtil by creating a new list
        }
    }

    companion object {
        private val COLOR_PILL_DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<ColorPill>() {
            override fun areItemsTheSame(oldItem: ColorPill, newItem: ColorPill) = oldItem.color == newItem.color && oldItem.index == newItem.index
            override fun areContentsTheSame(oldItem: ColorPill, newItem: ColorPill) = oldItem.isSelected == newItem.isSelected
            override fun getChangePayload(oldItem: ColorPill, newItem: ColorPill): Any? {
                if (oldItem.index == newItem.index && oldItem.isSelected != newItem.isSelected)
                    return ColorPillViewHolder.SELECTED_STATE_UPDATE
                return super.getChangePayload(oldItem, newItem)
            }
        }
    }
}
