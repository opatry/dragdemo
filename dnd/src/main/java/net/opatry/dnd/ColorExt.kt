package net.opatry.dnd

import androidx.annotation.ColorInt
import kotlin.math.sqrt

private const val RED_PERCEPTION = 0.299f
private const val GREEN_PERCEPTION = 0.587f
private const val BLUE_PERCEPTION = 0.114f

const val BRIGHTNESS_THRESHOLD = 0.75f

/*
 * cf. http://alienryderflex.com/hsp.html
 *
 * Given R, G, B between 0 & 1
 * perceived brightness = sqrt( .299 * R^2 + .587 * G^2 + .114 * B^2 )
 *
 * The three constants (.299, .587, and .114) represent the different degrees to which each of the primary (RGB)
 * colors affects human perception of the overall brightness of a color.  Notice that they sum to 1.
 */
fun getPerceivedBrightness(@ColorInt color: Int): Float {
    // implementation without using android.graphics.Color to allow unit test on JVM
    val alpha = (color shr 24 and 0xff) / 255f
    val red = (color shr 16 and 0xff) / 255f
    val green = (color shr 8 and 0xff) / 255f
    val blue = (color and 0xff) / 255f

    val redFactor = (RED_PERCEPTION * red * red).toDouble()
    val greenFactor = (GREEN_PERCEPTION * green * green).toDouble()
    val blueFactor = (BLUE_PERCEPTION * blue * blue).toDouble()

    return 1 - alpha * (1 - sqrt(redFactor + greenFactor + blueFactor)).toFloat()
}

@JvmOverloads
fun <T> getBrightnessAwareValue(@ColorInt color: Int,
                                valueIfLight: T,
                                valueIfDark: T,
                                threshold: Float = BRIGHTNESS_THRESHOLD) =
        if (getPerceivedBrightness(color) < threshold) valueIfLight
        else valueIfDark
