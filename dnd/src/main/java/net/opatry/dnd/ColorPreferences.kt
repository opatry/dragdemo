package net.opatry.dnd

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.ColorInt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val PREFERENCE_COLOR_DELIMITER = ","

fun SharedPreferences.Editor.putColors(key: String, @ColorInt colors: List<Int>): SharedPreferences.Editor {
    return putString(key, colors.joinToString(PREFERENCE_COLOR_DELIMITER))
}

@ColorInt
fun SharedPreferences.getColors(key: String, @ColorInt defaultColors: List<Int> = listOf()): List<Int> {
    val strColors = getString(key, null)
    if (strColors.isNullOrEmpty()) return defaultColors
    return strColors.split(PREFERENCE_COLOR_DELIMITER).map { it.toInt() }
}

fun SharedPreferences.store(instructions: SharedPreferences.Editor.() -> Unit) {
    with (edit()) {
        instructions()
        apply()
    }
}

private const val COLORS_PREFERENCES_NAME = "com.myscript.nebo.InkSettingsPrefs" // DO NOT MODIFY, used as shared pref file
private const val COLORS_KEY = "INK_SETTINGS_COLORS_KEY" // DO NOT MODIFY, used as shared pref key

class ColorPreferences(application: Application, @ColorInt defaultColors: List<Int>) {

    private val prefs: SharedPreferences = application.getSharedPreferences(COLORS_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @ColorInt
    var colors: List<Int> = listOf()
        set(value) {
            field = value
            storeColors()
        }

    init {
        colors = prefs.getColors(COLORS_KEY, defaultColors)
    }

    @ColorInt
    fun resetColors(@ColorInt defaultColors: List<Int> = listOf()): List<Int> {
        colors = defaultColors
        return colors
    }

    @ColorInt
    fun removeColorAt(colorIndex: Int): List<Int> {
        if (colorIndex in colors.indices) {
            colors = mutableListOf<Int>().apply {
                addAll(colors)
                removeAt(colorIndex)
            }
        }
        return colors
    }

    @ColorInt
    fun addColor(@ColorInt color: Int): List<Int> {
        colors = mutableListOf<Int>().apply {
            addAll(colors)
            add(color)
        }
        return colors
    }

    @ColorInt
    fun duplicateColorAt(colorIndex: Int): List<Int> {
        if (colorIndex in colors.indices) {
            colors = mutableListOf<Int>().apply {
                addAll(colors)
                add(colorIndex, colors[colorIndex])
            }
        }
        return colors
    }

    @ColorInt
    fun moveColors(fromIndex: Int, toIndex: Int): List<Int> {
        if (fromIndex in colors.indices && toIndex in colors.indices) {
            colors = mutableListOf<Int>().apply {
                addAll(colors)
                val color = removeAt(fromIndex)
                add(toIndex, color)
            }
        }
        return colors
    }

    private fun storeColors() {
        if (!prefs.contains(COLORS_KEY) && colors.isEmpty()) return
        with (colors) {
            GlobalScope.launch {
                withContext(Dispatchers.Default) {
                    prefs.store {
                        putColors(COLORS_KEY, this@with)
                    }
                }
            }
        }
    }
}
