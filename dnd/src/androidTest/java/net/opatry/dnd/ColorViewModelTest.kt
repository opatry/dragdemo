package net.opatry.dnd

import android.app.Application
import androidx.annotation.ColorInt
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.junit.runner.RunWith
import java.lang.Exception
import kotlin.coroutines.ContinuationInterceptor

/**
 * Sets the main coroutines dispatcher to a [TestCoroutineScope] for unit testing. A
 * [TestCoroutineScope] provides control over the execution of coroutines.
 *
 * Declare it as a JUnit Rule:
 *
 * ```
 * @get:Rule
 * var mainCoroutineRule = MainCoroutineRule()
 * ```
 *
 * Use it directly as a [TestCoroutineScope]:
 *
 * ```
 * mainCoroutineRule.pauseDispatcher()
 * ...
 * mainCoroutineRule.resumeDispatcher()
 * ...
 * mainCoroutineRule.runBlockingTest { }
 * ...
 *
 * ```
 */
@ExperimentalCoroutinesApi
class MainCoroutineRule : TestWatcher(), TestCoroutineScope by TestCoroutineScope() {

    override fun starting(description: Description?) {
        super.starting(description)
        Dispatchers.setMain(this.coroutineContext[ContinuationInterceptor] as CoroutineDispatcher)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        Dispatchers.resetMain()
    }
}

@ColorInt
private val defaultColors = listOf(
        0x35b009,
        0x4d4cc8,
        0x9be273,
        0xa6b16d,
        0x000000,
        0x9e503c,
        0xfbebc9,
        0xd8af37,
        0x506993,
        0x474e3e,
        0x72c00c,
        0xe14126,
        0xe8dd5d,
        0xe727f0,
        0x7346d0,
        0x455152,
        0x78fbcd,
        0x2e9ec1,
        0x8e7c46,
)

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class ColorViewModelTest {

    // Run tasks synchronously
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    // Sets the main coroutines dispatcher to a TestCoroutineScope for unit testing.
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val application: Application
        get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application

    private fun usingViewModel(@ColorInt colors: List<Int> = defaultColors, selectedIndex: Int = 0, test: (ColorViewModel) -> Unit) {
        with(ColorViewModel(ColorPreferences(application, colors))) {
            try {
                resetColors(colors)
                selectColorAt(selectedIndex)
                test(this)
            } finally {
                onCleared()
            }
        }
    }

    @Test
    fun getColors() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d)) { vm ->
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), colors?.map { it.color })
        }
    }

    @Test
    fun resetColors() = usingViewModel { vm ->
        vm.resetColors(listOf())
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf<ColorPill>(),colors)
        }
        vm.resetColors(defaultColors)
        vm.colors.observeForTesting { colors ->
            assertEquals(defaultColors, colors?.map { it.color })
        }
    }

    @Test
    fun selectAtSelectedColor() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColorAt(0)
        vm.colors.observeForTesting { colors ->
            assertEquals(0x35b009, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun selectColorAtInvalidIndexSelectsNothing() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColorAt(50)
        vm.colors.observeForTesting { colors ->
            assertTrue(colors?.none { it.isSelected } ?: false)
        }
    }

    @Test
    fun selectColorAt() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColorAt(2)
        vm.colors.observeForTesting { colors ->
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun selectColorSelectedKeepSelection() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColor(0x35b009)
        vm.colors.observeForTesting { colors ->
            assertEquals(0x35b009, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun selectColorInvalidColorSelectsNothing() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColor(0x000000)
        vm.colors.observeForTesting { colors ->
            assertTrue(colors?.none { it.isSelected } ?: false)
        }
    }

    @Test
    fun selectColor() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 0) { vm ->
        vm.selectColor(0x4d4cc8)
        vm.colors.observeForTesting { colors ->
            assertEquals(0x4d4cc8, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun removeColor() = usingViewModel(listOf(0x35b009, 0x4d4cc8)) { vm ->
        vm.removeColor(0x35b009)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x4d4cc8), colors?.map { it.color })
            assertEquals(0x4d4cc8, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun removeColorAtInvalidIndex() = usingViewModel(listOf(0x35b009, 0x4d4cc8), selectedIndex = 1) { vm ->
        vm.removeColorAt(50)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x4d4cc8), colors?.map { it.color })
            assertEquals(0x4d4cc8, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun removeColorAt() = usingViewModel(listOf(0x35b009, 0x4d4cc8)) { vm ->
        vm.removeColorAt(1)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009), colors?.map { it.color })
            assertEquals(0x35b009, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun addColor() = usingViewModel(listOf(0x35b009)) { vm ->
        vm.addColor(0x8e7c46)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x8e7c46), colors?.map { it.color })
            assertEquals(0x8e7c46, colors?.first { it.isSelected }?.color)
        }
    }

    @Test
    fun addRandomColor() = usingViewModel(listOf(0x35b009)) { vm ->
        vm.addColor()
        vm.colors.observeForTesting { colors ->
            assertEquals(2, colors?.size)
            val rawColors = colors?.map { it.color }
            assertEquals(rawColors, rawColors?.distinct())
        }
        vm.addColor()
        vm.colors.observeForTesting { colors ->
            assertEquals(3, colors?.size)
            val rawColors = colors?.map { it.color }
            assertEquals(rawColors, rawColors?.distinct())
        }
    }

    @Test
    fun duplicateSelectedColor() = usingViewModel(listOf(0x35b009), selectedIndex = 0) { vm ->
        vm.duplicateSelectedColor()
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x35b009), colors?.map { it.color })
            assertEquals(0x35b009, colors?.first { it.isSelected }?.color)
            assertEquals(1, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorBeforeSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 3) { vm ->
        vm.moveColor(0, 1)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x4d4cc8, 0x35b009, 0x9be273, 0xa6b16d), colors?.map { it.color })
            assertEquals(0xa6b16d, colors?.first { it.isSelected }?.color)
            assertEquals(3, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorRightBeforeSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(0, 1)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x4d4cc8, 0x35b009, 0x9be273, 0xa6b16d), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(2, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorRightAfterSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(0, 2)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x4d4cc8, 0x9be273, 0x35b009, 0xa6b16d), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(1, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorAfterSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(0, 3)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x4d4cc8, 0x9be273, 0xa6b16d, 0x35b009), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(1, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorFromAfterToBeforeSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(3, 0)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0xa6b16d, 0x35b009, 0x4d4cc8, 0x9be273), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(3, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorFromAfterToRightBeforeSelected() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(3, 2)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x4d4cc8, 0xa6b16d, 0x9be273), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(3, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveSelectedColorBefore() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(2, 0)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x9be273, 0x35b009, 0x4d4cc8, 0xa6b16d), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(0, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveSelectedColorAfter() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(2, 3)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x4d4cc8, 0xa6b16d, 0x9be273), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(3, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun moveColorNOP() = usingViewModel(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), selectedIndex = 2) { vm ->
        vm.moveColor(0, 0)
        vm.colors.observeForTesting { colors ->
            assertEquals(listOf(0x35b009, 0x4d4cc8, 0x9be273, 0xa6b16d), colors?.map { it.color })
            assertEquals(0x9be273, colors?.first { it.isSelected }?.color)
            assertEquals(2, colors?.indexOfFirst { it.isSelected })
        }
    }

    @Test
    fun removeLastColor() = usingViewModel(listOf(0x35b009)) { vm ->
        try {
            vm.removeColorAt(0)
            vm.colors.observeForTesting { colors ->
                assertTrue(colors?.isEmpty() ?: false)
            }
        } catch (e: Exception) {
            fail(e.message)
        }
    }
}
