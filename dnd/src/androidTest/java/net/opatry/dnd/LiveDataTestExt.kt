package net.opatry.dnd

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

fun <T> LiveData<T>.observeForTesting(block: (T?) -> Unit) {
    val observer = Observer<T> { }
    observeForever(observer)
    try {
        block(value)
    } finally {
        removeObserver(observer)
    }
}
